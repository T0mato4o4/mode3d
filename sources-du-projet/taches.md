- [x] MVC (à vérifier)

- Affichage : Thomas
    - [ ] Faces
    - [ ] Segments
    - [ ] Faces + segments

- Affichage secondaire : Adil
    - [x] Tabpane
    - [ ] Options d'affichage

- Gestion de la bibliothèque de modèle : Adil
    - [x] Recherche de modèle (modification du parser: optimisation)
    - [x] Edition des informations (création d'un "saver")
    - [x] Trier le tableau
    - [x] Contrôleur horloge

- Fonctionnalités au choix : Clotaire
    - [x] Modèle centré
    - [x] Eclairage
    - [ ] Vue en tranches (abandonné)
    - [ ] Lissage
    - [ ] Ombre portée (abandonné)

- Doc : tous
    - [ ] UML
    - [ ] Javadoc
    - [ ] Video