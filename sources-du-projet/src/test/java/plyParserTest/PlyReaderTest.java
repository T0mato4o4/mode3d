package plyParserTest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;

import org.junit.jupiter.api.Test;

import plyParser.FileNotFoundException;
import plyParser.PlyReader;

class PlyReaderTest {
	private final File TESTFILE = new File("ressources/plyReaderTest/invalidFile/plyReaderTest1");
	private final File FILE_NOT_FOUND = new File("invalide_path");

	@Test
	void instanciateTest() {
		assertThrows(FileNotFoundException.class, ()->{
			new PlyReader(FILE_NOT_FOUND);
		});

		assertDoesNotThrow(() -> {
			new PlyReader(TESTFILE);
		});
	}
	
	@Test
	void getLineTest() {
		PlyReader reader = null;
		try {
			reader = new PlyReader(TESTFILE);

			StringBuilder sb = new StringBuilder();

			while (reader.hasNext()) sb.append(reader.getLine() + "\n");

			assertEquals("ply\n" +
					"format ascii 1.0\n" +
					"comment useless comment but hey, just to test things ya know\n" +
					"comment this file is a cube\n" +
					"element vertex 8\n" +
					"and so on...\n", sb.toString());
		} catch (plyParser.FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	@Test
	void getLineWithoutCommentTest() {
		try {
			PlyReader reader = new PlyReader(TESTFILE);

			StringBuilder sb = new StringBuilder();

			while (reader.hasNext()) sb.append(reader.getLineWithoutComment() + "\n");

			assertEquals("ply\n" +
					"format ascii 1.0\n" +
					"element vertex 8\n" +
					"and so on...\n", sb.toString());
		} catch (plyParser.FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
