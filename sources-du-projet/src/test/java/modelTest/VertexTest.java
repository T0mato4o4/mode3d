package modelTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import model.Vertex;

public class VertexTest {

	@Test
	void getNormeTest() {
		Vertex toTest = new Vertex(2, -2, -1);
		
		assertEquals(3, toTest.getNorme());
	}
}
