package modelTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import model.Polygon;
import model.Vertex;

public class PolygonTest {
	
	// valeurs de tests recuperees sur la correction du td
	@Test
	void getVectNormUnitTest() {
		Polygon toTest = new Polygon(new Vertex(1,2,3),
				new Vertex(2,3,3),
				new Vertex(2,2,5));
		
		Vertex expected = new Vertex(2.0/3.0, -2.0/3.0, -1.0/3.0);
		
		assertEquals(expected, toTest.getVectNormUnit());
	}
}
