package modelTest;

import model.Vertex;

public class VertexBenchmark {
    private void benchmark() {
        System.out.println("Slow vertex");
        int times = 5_000_000;
        Vertex[] vertices = new Vertex[times];

        long startTime = System.currentTimeMillis();
        // Intenciation time
        for (int i = 0; i < times; i++) {
            vertices[i] = new Vertex(0.0,1.0,2.0);
        }
        System.out.println("Intenciation time: " + (System.currentTimeMillis() - startTime) + "ms");


        startTime = System.currentTimeMillis();
        // Get time
        double[][] values = new double[times][3];
        for (int i = 0; i < times; i++) {
            values[i][0] = vertices[i].getX();
            values[i][1] = vertices[i].getY();
            values[i][2] = vertices[i].getZ();
        }
        System.out.println("Get time: " + (System.currentTimeMillis() - startTime) + "ms");


        startTime = System.currentTimeMillis();
        // Set time
        for (int i = 0; i < times; i++) {
            vertices[i].setX(1.0);
            vertices[i].setY(2.0);
            vertices[i].setZ(3.0);
        }
        System.out.println("Set time: " + (System.currentTimeMillis() - startTime) + "ms");
    }
}
