package controller;

import javafx.event.EventHandler;
import javafx.scene.input.ScrollEvent;
import maths.Calculator;
import maths.Homothetie;
import maths.Translation;
import model.Figure;
import model.Vertex;

/**
 * Listener pour lorsque la souris est scrolee, effectue un zoom / dezoom
 *
 */
public class MouseScrolledListener implements EventHandler<ScrollEvent> {
	private FigureController figureController;

	/**
	 * stocke le figureController necessaire
	 * @param figureController figureController qui sera utilise
	 */
	public MouseScrolledListener(FigureController figureController) {
		this.figureController = figureController;
	}

	@Override
	public void handle(ScrollEvent event) {
		figureController.zoom(
				event.getDeltaY() < 0 ? FigureController.ZOOM.OUT : FigureController.ZOOM.IN
		);
	}
}
