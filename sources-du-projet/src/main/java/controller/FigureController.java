package controller;

import java.util.Observer;
import java.util.function.Consumer;

import decoupeur.Trancheur;
import maths.Calculator;
import maths.Homothetie;
import maths.TransRotaX;
import maths.TransRotaY;
import maths.TransRotaZ;
import maths.Transformation;
import maths.Translation;
import model.Figure;
import model.OmbreFigure;
import model.Polygon;
import model.Vertex;

public class FigureController {
    private Figure figure;
  
    public enum ZOOM {IN, OUT}

    private final double ZOOM_IN = 1.10;
    private final double ZOOM_OUT = 0.90;
    private final double MAX_ZOOM = 1800.0;
    private final double MIN_ZOOM = 100;

    private final double RAPPORT_ROTATION = 300.0;

    public FigureController(Figure figure) {
        this.figure = figure;
    }

    public void zoom(ZOOM type) {
        Vertex posF = figure.getTmpCentreGravite();
        if (type == ZOOM.OUT && (figure.getFigureXLength() > this.MIN_ZOOM || figure.getFigureYLength() > this.MIN_ZOOM)) {
            Calculator.calculate(figure, new Homothetie(ZOOM_OUT));
        }
        if (type == ZOOM.IN && (figure.getFigureXLength() < this.MAX_ZOOM || figure.getFigureYLength() < this.MAX_ZOOM)) {
            Calculator.calculate(figure, new Homothetie(ZOOM_IN));
        }
        Calculator.calculate(figure, new Translation(posF.moinsVertex(figure.getTmpCentreGravite())));
        
    }

    public void translate(double x, double y) {
        Calculator.calculate(figure, new Translation(new Vertex(x, y, 0)));
        figure.updateCentreGravite();
    }


    /**
     * Effectue une rotation de par rapport � y et x (valeur arbitraire)
     *
     * @param yPos
     * @param xPos
     */
    public void rotate(double xPos, double yPos, double zPos) {
        Vertex centreF = Calculator.calculateCopy(figure.getCentreGravite(), new Transformation[0]);
        if (yPos != 0) {
            Calculator.calculate(figure, new TransRotaX(centreF, yPos / RAPPORT_ROTATION));
        }
        if (xPos != 0) {
            Calculator.calculate(figure, new TransRotaY(centreF, -xPos / RAPPORT_ROTATION));
        }
        if (zPos != 0) {
            Calculator.calculate(figure, new TransRotaZ(centreF, zPos / RAPPORT_ROTATION));
        }
    }

    /**
     * Effectue une rotation autour de l'axe Y
     *
     * @param yPos
     */
    public void rotateY(double yPos) {
        rotate(0, yPos, 0);
    }

    /**
     * Effectue une rotation autour de l'axe X
     *
     * @param xPos
     */
    public void rotateX(double xPos) {
        rotate(xPos, 0, 0);
    }

    /**
     * Effectue une rotation autour de l'axe Z
     *
     * @param zPos
     */
    public void rotateZ(double zPos) {
        rotate(0, 0, zPos);
    }


    private static final int MARGE = 80;

    /**
     * centre la figure par rapport au canvas et la r�duit
     */
    public void normalize(double canvasWidth, double canvasHeight) {
        double rapport;
        double widthMarge = canvasWidth-MARGE;
        double heightMarge = canvasHeight-MARGE;

        if(figure.getFigureXLength() > figure.getFigureYLength()) {
            rapport = figure.getFigureXLength() / widthMarge;
        }
        else {
            rapport = figure.getFigureYLength() / heightMarge;
        }

        rapport = 1.0/rapport;
        Calculator.calculate(figure, new Homothetie(rapport));

        double rapportCanvas = 1.0;

        if(figure.getFigureXLength() > canvasWidth) {
            rapportCanvas = figure.getFigureXLength() / widthMarge;
        }
        else if(figure.getFigureYLength() > canvasHeight){
            rapportCanvas = figure.getFigureYLength()/heightMarge;
        }
        rapportCanvas = 1.0/rapportCanvas;


        if(rapportCanvas != 1.0) {
            Calculator.calculate(figure, new Homothetie(rapportCanvas));
        }

    }

    /**
     * centre la figure
     */
    public void centrage(double canvasWidth, double canvasHeight) {
        Vertex centreCanvas = new Vertex(canvasWidth/2, canvasHeight/2, 0);
        Vertex translation = centreCanvas.moinsVertex(figure.getTmpCentreGravite());
        Calculator.calculate(figure, new Translation(translation));

        figure.updateCentreGravite();
        figure.update();
    }

    /**
     * Ajoute un observer à la figure
     * @param canvasController
     */
    public void addObserver(Observer canvasController) {
        figure.addObserver(canvasController);
    }

    public int numberOfPolygons() {
        return figure.getPolygons().size();
    }

    public void forEachPolygons(Consumer<Polygon> polygonConsumer) {
    	figure.getPolygons().forEach(polygonConsumer);
    }

}