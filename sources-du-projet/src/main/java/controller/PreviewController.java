package controller;

import javafx.beans.value.ChangeListener;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.CheckMenuItem;
import model.Figure;
import model.FigureInfo;
import plyParser.Parser;
import thread.AutoRotation;

public class PreviewController {
    private final CheckMenuItem rotationAuto;
    private final Canvas previewCanvas;

    private FigureInfo figureInfo;
    private Figure f;

    private Service<Figure> previewRendering;
    private AutoRotation rotaX;
    private AutoRotation rotaY;

    private ChangeListener<Boolean> rotationAutoListener;

    public PreviewController(Canvas canvas, CheckMenuItem rotationAuto) {
        this.previewCanvas = canvas;
        this.rotationAuto = rotationAuto;
    }

    private void preview() {
        if(previewRendering != null && previewRendering.getState() == Worker.State.RUNNING) {
            previewRendering.cancel();
            System.out.println("Preview canceled");
        }

        Service<Void> previewRendering = new Service<>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<>() {
                    @Override
                    protected Void call() {
                        if(f != null) {
                            try {
                                f.finalize();
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        }



                        f = new Parser(figureInfo.getFile()).getFigure();
                        FigureController figureController = new FigureController(f);
                        new CanvasController(previewCanvas, figureController, false);

                        if(rotaX != null)
                            rotaX.stop();

                        rotaX = new AutoRotation(figureController::rotateX, 3);

                        if(rotaY != null)
                            rotaY.stop();

                        rotaY = new AutoRotation(figureController::rotateY, 3);


                        // Activation en fonction du menuitem
                        if(rotationAuto.isSelected()) {
                            rotaX.start();
                            rotaY.start();
                        }

                        if(rotationAutoListener != null)
                            rotationAuto.selectedProperty().removeListener(rotationAutoListener);

                        rotationAutoListener = (observable1, oldValue1, newValue1) -> {
                            if(newValue1) {
                                rotaX.start();
                                rotaY.start();
                            } else {
                                rotaX.stop();
                                rotaY.stop();
                            }
                        };

                        rotationAuto.selectedProperty().addListener(rotationAutoListener);

                        return null;
                    }
                };
            }
        };

        previewRendering.start();
    }

    public void setInfo(FigureInfo info) {
        this.figureInfo = info;
        preview();
    }
}
