package plyParser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Classe regroupant les fonctions permettant la récupération d'information relative au fichier ply dans une chaine.
 */
public class Find {
    private ParserHelper parserHelper;
    private PlyReader pR;

    private final static Pattern vertexElementPattern = Pattern.compile("(?!element vertex )\\d+");
    private final static Pattern propertyNamePattern = Pattern.compile("\\w+$");
    private final static Pattern elementFacesPattern = Pattern.compile("\\d+$");
    private final static Pattern verticesPattern = Pattern.compile("-?\\d+(\\.\\d+)?((e|E)-\\d+)?");
    private final static Pattern verticesNumberPattern = Pattern.compile("^\\d+");
    private final static Pattern facesPattern = Pattern.compile("(?!^\\d+)\\d+");

    public Find(PlyReader pR, ParserHelper parserHelper) {
        this.parserHelper = parserHelper;
        this.pR = pR;
    }

    /**
     * Recherche le nombre de vertex définit dans la validateVertexElement.
     * @param validateVertexElement la chaine contenant le nombre de vertex de la figure
     * @return le nombre de vertex de la figure
     */
    String vertexElement(String validateVertexElement) {
        return parserHelper.find(vertexElementPattern, validateVertexElement);
    }

    /**
     * Recherche le nom de la propriété dans validateProperty.
     * @param validateProperty la chaine contenant le nom de la propriété
     * @return le nom de la propriété
     */
    String propertyName(String validateProperty) {
        return parserHelper.find(propertyNamePattern, validateProperty);
    }

    /**
     * Recherche le nombre de faces définit dans validateElementFaces.
     * @param validateElementFaces la chaine contenant le nombre de faces
     * @return le nombre de faces
     */
    String elementFaces(String validateElementFaces) {
        return parserHelper.find(elementFacesPattern, validateElementFaces);
    }

    /**
     * Recherche les coordonnées du vertrex.
     * @param validateVertice la chaine contenant les coordonnées du vertrex
     * @return les coordonnées du vertrex
     */
    ArrayList<String> vertices(String validateVertice) {
    	return parserHelper.findAll(verticesPattern, validateVertice);
    }

    /**
     * Recherche le nombre de vertex dans la définition d'un polygon
     * Exemple: 3 0 1 2 => retourne 3
     * @param line la chaine contenant le nombre de vertex
     * @return le nombre de vertex
     */
    String verticesNumber(String line) {
        return parserHelper.find(verticesNumberPattern, line);
    }

    /**
     * Retourne les numéros des vertex du polygon.
     * Lève une exception si le nombre de vertex ne correspond pas.
     * @bug l'expression régulière ne supporte pas les espaces en début de chaine
     * @param line la chaine contenant les numéros des vertex du polygon
     * @return les numéros des vertex du polygon
     */
    ArrayList<String> faces(String line) {
        ArrayList<String> vertexNumbers = parserHelper.findAll(facesPattern, line);

        // Vérifie la dupliquation des vertex
        Set<String> set = new HashSet<>();
        for (String vertexNumber : vertexNumbers) {
            if (set.contains(vertexNumber)) {
                throw new WrongFileFormatException("Le vertex numéro " + vertexNumber + " est déjà utilisé dans le polygon à la ligne" + pR.lineNumber());
            } else set.add(vertexNumber);
        }

        return vertexNumbers;
    }
    
    
    /**
     * Teste si le commentaire match le mot toMatch et renvoie la valeur liee
     * @param comment le commentaire a verifier
     * @param toMatch la valeur a verifier
     * @return si le commentaire est valide, renvoie la valeur liee a toMatch, vide sinon
     */
    private String getCommentProperty(String comment, String toMatch) {
    	if(comment.matches("\\s*comment "+toMatch+":(\\S*\\s*)+")) {
    		return comment.substring(comment.indexOf(':')+1);
    	}
    	return "";
    }
    
    /**
     * verifie que le commentaire est le nom d'auteur et le renvoie
     * @param comment le commentaire a verifier
     * @return le nom de l'auteur
     */
    String auteur(String comment) {
    	return this.getCommentProperty(comment, "auteur");
    }
    
    /**
     * verifie que le commentaire est le nom de la figure et la renvoie
     * @param comment le commentaire a verifier
     * @return le nom de la figure
     */
    String nom(String comment) {
    	return this.getCommentProperty(comment, "nom");
    }
    
    /**
     * verifie que le commentaire est la description et la renvoie
     * @param comment le commentaire a verifier
     * @return la description
     */
    String description(String comment) {
    	return this.getCommentProperty(comment, "description");
    }
    
    /**
     * verifie que le commentaire est la date de creation et la renvoie
     * @param comment le commentaire a verifier
     * @return la date de creation
     */
    String dateCreation(String comment) {
    	return this.getCommentProperty(comment, "dateCreation");
    }
}
