package plyParser;

import java.util.regex.Pattern;

/**
 * La classe Validate regroupe les fonctions permettant la validation du fichier ply.
 */
public class Validate {
    private final ValidationHelper validationHelper;
    private final PlyReader pR;

    private final static Pattern plyPattern = Pattern.compile(" *^ply *");
    private final static Pattern formatPattern = Pattern.compile("format ascii 1\\.0");
    private final static Pattern vertexElementPattern = Pattern.compile("element vertex \\d+");
    private final static Pattern propertyPattern = Pattern.compile("property (char|short|int|uchar|ushort|uint|float|double)(32|64|8|16)? \\w+");
    private final static Pattern elementFacesPattern = Pattern.compile("element face \\d+");
    private final static Pattern propertyListPattern = Pattern.compile("property list ((char|short|int|uchar|ushort|uint|float|double)(32|64|8|16)? ){2}\\w*");
    private final static Pattern endHeaderPattern = Pattern.compile("end_header");
    private final static Pattern isProperty = Pattern.compile("property.*");

    public Validate(PlyReader pR, ValidationHelper validationHelper) {
        this.pR = pR;
        this.validationHelper = validationHelper;
    }

    /**
     * Vérifie que le fichier commence par "ply".
     */
    void ply() {
        validationHelper.validate(plyPattern, pR.getLine());
    }

    /**
     * Vérifie que le format du fichier est "ascii 1.0".
     */
    void format() {
        validationHelper.validate(formatPattern, pR.getLineWithoutComment());
    }

    /**
     * Vérifie la définition du nombre de vertex.
     * @return la chaine vérifier
     */
    String vertexElement() {
        return validationHelper.validate(vertexElementPattern, pR.getLineWithoutComment());
    }

    /**
     * Vérifie la définition d'une propriété avec son type et son nom.
     * @param line la chaine à vérifier
     * @return la chaine vérifiée
     */
    String property(String line) {
        return validationHelper.validate(propertyPattern, line);
    }

    /**
     * Vérifie la définition du nombre face.
     * @param line la chaine à vérifier
     * @return la chaine vérifiée
     */
    String elementFaces(String line) {
        return validationHelper.validate(elementFacesPattern, line);
    }

    /**
     * Vérifie la définition des types de la liste des propriétées.
     */
    void propertyList() {
        validationHelper.validate(propertyListPattern, pR.getLineWithoutComment());
    }

    /**
     * Véréfie que le header finit par "end_header".
     */
    void endHeader() {
        validationHelper.validate(endHeaderPattern, pR.getLineWithoutComment());
    }

    /**
     * Vérifie la définition d'un vertex.
     * @param propertyNumber le nombre de propriétés du vertexe.
     * @return la chaine vérifiée.
     */
    String vertice(int propertyNumber) {
        Pattern pattern = Pattern.compile("(-?\\d+(\\.\\d+)?(e-\\d+)? ){" + propertyNumber + "}(-?\\d+(\\.\\d+)?(e-\\d+)?) ?");
        return validationHelper.validate(pattern, pR.getLine());
    }

    /**
     * Vérifie la définition d'un polygon.
     * @param line la définition d'un polygon
     * @param numberOfVertex le nombre du vertex du polygon
     * @return la chaine vérifiée.
     */
    String faces(String line, int numberOfVertex) {
        Pattern pattern = Pattern.compile("(\\d+)( \\d+){" + numberOfVertex + "} *");
        return validationHelper.validate(pattern, line);
    }

    /**
     * Vérifie que la ligne est une propriété.
     * @param line la ligne à vérifier
     * @return est une propiété
     */
    boolean isProperty(String line) {
        try {
            validationHelper.validate(isProperty, line);
            return true;
        } catch (WrongFileFormatException e) {
            return false;
        }
    }
}
