package plyParser;

import model.Figure;
import model.Polygon;
import model.Vertex;

import java.io.*;
import java.util.ArrayList;

public class PlyWriter {
    Figure figure;

    public PlyWriter(Figure figure) {
        this.figure = figure;
    }

    public void write(File file) {
        try {
            FileWriter fileWriter = new FileWriter(file);

            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write("ply\nformat ascii 1.0\n");
            if(figure.getInformations().getAuthor() != null)
                bufferedWriter.write("comment author: " + figure.getInformations().getAuthor() + "\n");

            if(figure.getInformations().getCreationDate() != null)
                bufferedWriter.write("comment dateCreation: " + figure.getInformations().getCreationDate() + "\n");

            if(figure.getInformations().getDescription() != null)
                bufferedWriter.write("comment description: " + figure.getInformations().getDescription() + "\n");

            if(figure.getInformations().getFigureName() != null)
                bufferedWriter.write("comment nom: " + figure.getInformations().getFigureName() + "\n");

            bufferedWriter.write("element vertex " + figure.getVertices().size() + "\n" +
                    "property float32 x\n" +
                    "property float32 y\n" +
                    "property float32 z\n");

            bufferedWriter.write("element face " + figure.getPolygons().size() + "\n" +
                    "property list uint8 int32 vertex_indices\n" +
                    "end_header\n");

            ArrayList<Vertex> vertices = figure.getVertices();
            for (Vertex v : vertices) {
                bufferedWriter.write(v.getX() + " " + v.getY() + " " + v.getZ() + "\n");
            }

            for (Polygon p: figure.getPolygons()) {
                bufferedWriter.write("3 " + vertices.indexOf(p.getVertices()[0]) + " "
                        + vertices.indexOf(p.getVertices()[1]) + " "
                        + vertices.indexOf(p.getVertices()[2]) + "\n");
            }
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
