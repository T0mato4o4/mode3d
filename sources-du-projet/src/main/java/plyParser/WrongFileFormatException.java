package plyParser;

import java.io.File;

public class WrongFileFormatException extends AlertBoxAbleException {
	private static final long serialVersionUID = 1L;
	
	public WrongFileFormatException(String errorMessage) {
		super(errorMessage);
	}
	
	public static WrongFileFormatException numberParsingError(String number, int lineNumber) {
		return new WrongFileFormatException("Erreur lors de l'analyse du nombre " + number + " à la ligne " + lineNumber);
	}

	public static WrongFileFormatException errorAtLine(String line, int lineNumber) {
		return new WrongFileFormatException("Erreur à la ligne " + lineNumber + " : " + line);
	}

	public static WrongFileFormatException errorAtLine(int lineNumber) {
		return new WrongFileFormatException("Erreur à la ligne " + lineNumber);
	}

	public String errorTitle() {
		return "Erreur lors de la lecture du fichier";
	}

	public String errorHeaderText(File file) {
		return "Erreur dans le fichier : " + file.getName();
	}
}
