package maths;

import model.Vertex;

/**
 * Calcul d'une homotethie
 * @author clotaire.dufresne.etu
 *
 */
public class Homothetie extends TransformationSimple {

	/**
	 * Stocke rapport pour utilisation dans calculate
	 * @param rapport rapport d'homothetie
	 */
	public Homothetie(Double rapport) {
		super(rapport);
	}

	
	@Override
	public void calculate(Vertex v) {
		Double k = (Double) super.value;
		
		double newX = v.getX()*k;
		double newY = v.getY()*k;
		double newZ = v.getZ()*k;
		
		super.setVertexNewValues(v, newX, newY, newZ);
	}

	@Override
	public void calculateVNU(Vertex vnu) {
		//inutile ici
	}

	@Override
	public String toString() {
		return "Homothetie{" +
				"rapport=" + value +
				'}';
	}

}
