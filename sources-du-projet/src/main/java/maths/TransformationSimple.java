package maths;

import model.Vertex;

/**
 * Classe abstraite qui definit les transformations
 * @author clotaire.dufresne.etu
 * 
 */
public abstract class TransformationSimple extends Transformation {
	
	protected Object value;
	
	/**
	 * Constructeur qui va permettre de stocker la valeur utilisee pour le calcul
	 * @param value valeur stockee pour le calcul
	 */
	protected TransformationSimple(Object value) {
		this.value = value;
	}

	
}