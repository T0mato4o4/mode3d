package maths;

import model.Vertex;

public class TransRotaX extends TransformationComplexe {

	/**
	 * Constructeur de rotation autour de l'axe Y sur le centre de l'objet
	 * @param centre vecteur du centre de l'objet
	 * @param angle valeur en radian de la rotation
	 */
	public TransRotaX(Vertex centre, double angle) {
		super(new Object[] {centre, angle});
	}
	
	@Override
	public void calculate(Vertex v) {
		double angle = (double) super.values[1];
		Vertex vTranslation = (Vertex) super.values[0];
		
		double newX = v.getX();
		double newY = Math.cos(angle) * v.getY() - Math.sin(angle) * v.getZ() - vTranslation.getY() * Math.cos(angle) + vTranslation.getZ() * Math.sin(angle) + vTranslation.getY();
		double newZ = Math.sin(angle) * v.getY() + Math.cos(angle) * v.getZ() - vTranslation.getY() * Math.sin(angle) - vTranslation.getZ() * Math.cos(angle) + vTranslation.getZ();;
		
		super.setVertexNewValues(v, newX, newY, newZ);		
	}
	
	@Override
	public void calculateVNU(Vertex vnu) {
		double angle = (double) super.values[1];
		(new RotationX(angle)).calculate(vnu);
	}

}
