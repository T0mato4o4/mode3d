package maths;

public abstract class TransformationComplexe extends Transformation {
	
	protected Object[] values;
	
	/**
	 * Constructeur qui va permettre de stocker les valeurs utilisees pour le calcul
	 * @param values valeurs stockees pour le calcul
	 */
	protected TransformationComplexe(Object...values) {
		this.values = values;
	}	
	

}
