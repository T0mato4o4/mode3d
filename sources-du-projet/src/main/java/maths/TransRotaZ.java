package maths;

import model.Vertex;

public class TransRotaZ extends TransformationComplexe {

	/**
	 * Constructeur de rotation autour de l'axe Y sur le centre de l'objet
	 * @param centre vecteur du centre de l'objet
	 * @param angle valeur en radian de la rotation
	 */
	public TransRotaZ(Vertex centre, double angle) {
		super(new Object[] {centre, angle});
	}
	
	@Override
	public void calculate(Vertex v) {
		double angle = (double) super.values[1];
		Vertex vTransition = (Vertex) super.values[0];
		
		Double newX = Math.cos(angle)*v.getX() - Math.sin(angle) * v.getY() - Math.cos(angle) * vTransition.getX() + Math.sin(angle) * vTransition.getY() + vTransition.getX();
        Double newY = Math.sin(angle)*v.getX() + Math.cos(angle)* v.getY() - Math.sin(angle) * vTransition.getX() - Math.cos(angle) * vTransition.getY() + vTransition.getY();
        Double newZ = v.getZ();
        
		super.setVertexNewValues(v, newX, newY, newZ);
	}
	
	@Override
	public void calculateVNU(Vertex vnu) {
		double angle = (double) super.values[1];
		(new RotationZ(angle)).calculate(vnu);
	}
	
}
