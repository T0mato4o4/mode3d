package maths;

import model.Vertex;

/**
 * Calcul d'une rotation par rapport a l'axe X
 * @author clotaire.dufresne.etu
 * 
 */
public class RotationX extends TransformationSimple{

	/**
	 * Permet de stocker la valeur qui sera utilisee au calcul
	 * @param angle angle en radian
	 */
	public RotationX(Object angle) {
		super(angle);
	}

	@Override
	public void calculate(Vertex v) {
		double radian = (double) super.value;
		
		double xValue = v.getX();
		double yValue = Math.cos(radian)*v.getY() + ((-1.0)*Math.sin(radian))*v.getZ();
		double zValue = Math.sin(radian)*v.getY() + Math.cos(radian)*v.getZ();
		
		super.setVertexNewValues(v, xValue, yValue, zValue);
	}

	@Override
	public void calculateVNU(Vertex vnu) {
		this.calculate(vnu);
	}
	
	@Override
	public String toString() {
		return "RotationX{" +
				"angle=" + value +
				'}';
	}
}
