package maths;

import model.Vertex;

/**
 * Calcul d'une translation
 * @author clotaire.dufresne.etu
 *
 */
public class Translation  extends TransformationSimple {

	/**
	 * Permet de stocker la valeur qui sera utilisee au calcul
	 * @param vertex utilisé pour la translation
	 */
	public Translation(Object vertex) {
		super(vertex);
	}

	@Override
	public void calculate(Vertex v) {
		Vertex t = (Vertex) super.value;
		
		double newX = v.getX() + t.getX();
		double newY = v.getY() + t.getY();
		double newZ = v.getZ() + t.getZ();
		
		super.setVertexNewValues(v, newX, newY, newZ);
	}
	
	@Override
	public void calculateVNU(Vertex vnu) {
		//inutile ici
	}

	@Override
	public String toString() {
		return "Translation{" +
				"vertex=" + (Vertex) value +
				'}';
	}
}
