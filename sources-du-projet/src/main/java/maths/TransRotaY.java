package maths;

import model.Vertex;

public class TransRotaY extends TransformationComplexe {
	
	/**
	 * Constructeur de rotation autour de l'axe Y sur le centre de l'objet
	 * @param centre vecteur du centre de l'objet
	 * @param angle valeur en radian de la rotation
	 */
	public TransRotaY(Vertex centre, double angle) {
		super(new Object[] {centre, angle});
	}

	@Override
	public void calculate(Vertex v) {
		double angle = (double) super.values[1];
		Vertex vTranslation = (Vertex) super.values[0];

		double newX = Math.cos(angle) * v.getX() - Math.sin(angle) * v.getZ() - Math.cos(angle) * vTranslation.getX() + Math.sin(angle) * vTranslation.getZ() + vTranslation.getX();
        double newY = v.getY();
        double newZ = Math.sin(angle) * v.getX() + Math.cos(angle) * v.getZ() - Math.sin(angle) * vTranslation.getX() - Math.cos(angle) * vTranslation.getZ() + vTranslation.getZ();
		
		super.setVertexNewValues(v, newX, newY, newZ);		
	}
	
	@Override
	public void calculateVNU(Vertex vnu) {
		double angle = (double) super.values[1];
		(new RotationY(angle)).calculate(vnu);
	}	

}
