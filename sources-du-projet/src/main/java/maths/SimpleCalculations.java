package maths;

import model.Vertex;

/**
 * Classe de calculs mathématiques simples
 * @author clotaire.dufresne.etu
 *
 */
public class SimpleCalculations {

	/**
	 * renvoie la moyenne de toutes les valeurs passees en parametres
	 * @param values ensemble de valeurs
	 * @return la moyenne des valeurs
	 */
	public static double average(Double...values) {
		double avg = 0;
		if(values == null) return avg;
		avg = SimpleCalculations.sum(values) / values.length;
		return avg;
	}

	/**
	 * calcule la somme de toutes les valeurs passees en parametre 
	 * @param values ensemble de valeurs 
	 * @return la somme des valeurs
	 */
	public static double sum(Double...values) {
		double sum = 0.0;
		
		if(values == null) return sum;
		
		for(int i = 0; i < values.length; i++) {
			if(values[i] != null) sum += values[i];
		}
		
		return sum;
	}
	
	/**
	 * calcule la somme de toutes les valeurs passees en parametre
	 * @param values ensemble de valeurs
	 * @return la somme des valeurs
	 */
	public static double sum(double...values) {
		Double[] valuesD = new Double[values.length];
		for(int i = 0; i < valuesD.length ; i++) {
			valuesD[i] = values[i];
		}
		return SimpleCalculations.sum(valuesD);
	}
	
	/**
	 * Calcule le produit vectoriel de deux vecteurs (x^y).
	 * @param vectX le vecteur x
	 * @param vectY le vecteur y
	 * @return le resultat de x^y
	 */
	public static Vertex produitVectoriel(Vertex vectX, Vertex vectY) {
		Vertex res = new Vertex(0, 0, 0);
		
		res.setX(vectX.getY()*vectY.getZ() - vectX.getZ()*vectY.getY());
		res.setY(vectX.getZ()*vectY.getX() - vectX.getX()*vectY.getZ());
		res.setZ(vectX.getX()*vectY.getY() - vectX.getY()*vectY.getX());
		
		return res;
	}
	
	public static void rendreUnitaire(Vertex aModif) {
		double norme = aModif.getNorme();
		if(norme != 1) {
			Calculator.calculate(aModif, new Homothetie(1.0/norme));
		}
	}
	
}
