package maths;

import model.Vertex;

/**
 * Calcul de la rotation par rapport a l'axe Z
 * @author Omegaslide
 *
 */
public class RotationZ extends TransformationSimple {

	public RotationZ(Object angle) {
		super(angle);
	}

	@Override
	public void calculate(Vertex v) {
		double radian = (double) super.value;
		
		double xValue= Math.cos(radian)*v.getX() + ((-1.0)*Math.sin(radian))*v.getY();
		double yValue= Math.sin(radian)*v.getX() + Math.cos(radian)*v.getY();
		double zValue= v.getZ();
		
		super.setVertexNewValues(v, xValue, yValue, zValue);
	}
	
	@Override
	public void calculateVNU(Vertex vnu) {
		this.calculate(vnu);
	}

	@Override
	public String toString() {
		return "RotationZ{" +
				"angle=" + value +
				'}';
	}
}
