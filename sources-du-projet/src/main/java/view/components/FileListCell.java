package view.components;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.io.File;

public class FileListCell extends ListCell<File> {
    private final TextField textField = new TextField();
    ListView<File> fileList;

    public FileListCell(ListView<File> fileList) {
        this.fileList = fileList;
    }

    @Override
    protected void updateItem(File file, boolean empty) {
        super.updateItem(file, empty);
        if (isEditing()) {
            textField.setText(file.getName());
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        } else {
            setContentDisplay(ContentDisplay.TEXT_ONLY);
            if (empty) {
                setText(null);
            } else {
                setText(file.getName());
            }
        }
    }
}
