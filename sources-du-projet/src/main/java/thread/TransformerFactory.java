package thread;

import java.util.ArrayList;

import maths.Transformation;
import model.Figure;

public class TransformerFactory {
	
	private static final int MAX_OPE_THREAD = 250;
	private Figure figure;
	private Transformation aAppliquer;
	
	public TransformerFactory(Figure figure, Transformation aAppliquer) {
		this.figure = figure;
		this.aAppliquer = aAppliquer;
	}
	
	public ArrayList<VerticesTransformer> makeThreads() {
		ArrayList<VerticesTransformer> res = new ArrayList<>();
		int reste = figure.getVertices().size();
		int deb = 0;
		int fin = MAX_OPE_THREAD;
		if(reste < MAX_OPE_THREAD) {
			fin = reste;
		}
		int nbThreads = reste / MAX_OPE_THREAD;
		if(nbThreads == 0 || reste % nbThreads != 0) {
			nbThreads++;
		}
		
		while(reste != 0){
			res.add(new VerticesTransformer(figure.getVertices(), deb, fin, aAppliquer));
			reste -= (fin - deb);
			if(reste < MAX_OPE_THREAD) {
				fin = fin + reste;
			}
			else {
				fin += MAX_OPE_THREAD;
			}
			deb += MAX_OPE_THREAD;
		}
		
		return res;
	}
}
