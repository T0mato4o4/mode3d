package thread;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import maths.Transformation;
import model.Vertex;

public class VerticesTransformer implements Callable<Void> {
	
	private ArrayList<Vertex> vertices;
	private int deb;
	private int fin;
	private Transformation aAppliquer;
	
	public VerticesTransformer(ArrayList<Vertex> vertices, int deb, int fin, Transformation aAppliquer) {
		super();
		this.vertices = vertices;
		this.deb = deb;
		this.fin = fin;
		this.aAppliquer = aAppliquer;
	}

	@Override
	public Void call() throws Exception {
		for(int idx = deb ; idx < fin ; idx++) {
			aAppliquer.calculate(vertices.get(idx));
		}
		return null;
	}
	
	
	
}
