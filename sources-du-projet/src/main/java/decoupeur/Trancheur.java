package decoupeur;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import maths.SimpleCalculations;
import model.Figure;
import model.Polygon;
import model.Vertex;

public class Trancheur {
	
	private Figure aTrancher;
	
	
	public Trancheur(Figure aTrancher) {
		this.aTrancher = aTrancher; 
	}
	
	/**
	 * renvoie une figure qui contient les faces NON FONCTIONNEL
	 * @param nbTranches le nombre de tranches souhaitees
	 * @return la figure contenant les tranches (non fonctionnel, il aurait fallu faire un tableau de tableaux de polygones puisqu'on peut avoir plusieurs faces par tranches)
	 */
	public Figure getTranches(int nbTranches) {
		Vertex vectXPlan = new Vertex(1,0,0);
		Vertex vectYPlan = new Vertex(0,1,0);
		Vertex normalePlan = SimpleCalculations.produitVectoriel(vectXPlan, vectYPlan);
		SimpleCalculations.rendreUnitaire(normalePlan);
		
		double pas = this.aTrancher.getFigureZLength() / (double) nbTranches;
		Double[] zExtremum = this.aTrancher.getExtremumValueVerticesZ();
		double zValue = zExtremum[0];
		ArrayList<Vertex[]> segmentsInter = new ArrayList<>();
		Vertex[][] vectSegments;
		ArrayList<Polygon> polygones = new ArrayList<>();		
		while(zValue < zExtremum[1]) {
			for(Polygon p : aTrancher.getPolygons()) {
				Vertex[] vertices = p.getVertices();
				vectSegments = new Vertex[][] {{vertices[0], vertices[1]},{vertices[0], vertices[2]},{vertices[1], vertices[2]}};
				ArrayList<Vertex[]> tmpSegments = (ArrayList<Vertex[]>) calculerInters(vectSegments, normalePlan, zValue);
				if(tmpSegments.size() == 3) {
					polygones.add(new Polygon(tmpSegments.get(0)[0], tmpSegments.get(0)[1], tmpSegments.get(1)[1]));
				}
				else {
					segmentsInter.addAll(tmpSegments);
				}
			}
			zValue += pas;
		}
		
//		for(Vertex[] v : segmentsInter) {
//			System.out.println("1 "+v[0]+"     2 "+v[1]);
//		}
		
		polygones.addAll(formerPolygones(segmentsInter));
		
		return new Figure(polygones);
	}
	

	private List<Vertex[]>calculerInters(Vertex[][] face, Vertex normalePlan, double zValue) {
		ArrayList<Vertex[]> intersections = new ArrayList<>();
		Vertex[] segment;
		Vertex intersection;
		int listIdx = 0;
		boolean initiated = false;
		for(int i = 0; i < face.length ; i++) {
			segment = face[i];
			if(segment[0].getZ() == zValue && segment[1].getZ() == zValue ) {
				intersections.add(new Vertex[] {segment[0], segment[1]});
			}
			else if(segment[0].getZ() != zValue && segment[1].getZ() != zValue ) {
				intersection = intersection(segment, normalePlan, zValue);
				if(initiated && intersection != null) {
					intersection = intersection(segment, normalePlan, zValue);
					intersections.get(listIdx)[1] = intersection;
					initiated = false;
					listIdx++;
				}
				else if(intersection != null){
					intersection = intersection(segment, normalePlan, zValue);
					intersections.add(new Vertex[] {intersection, null});
					initiated = true;
				}
			}

		}
		nettoyage(intersections);
		return intersections;
	}

	
	private void nettoyage(ArrayList<Vertex[]> intersections) {
		int maxIdx = intersections.size();
		for(int i = 0 ; i < maxIdx ; i++) {
			if(intersections.get(i)[1] == null || intersections.get(i)[0] == null) {
				intersections.remove(i);
				maxIdx--;
				i--;
			}
		}
	}


	private Vertex intersection(Vertex[] segment, Vertex normalePlan, double zValue) {
		Vertex a = segment[0];
		Vertex b = segment[1];
		double tau = (normalePlan.getX()*(a.getX() - b.getX()) + normalePlan.getY()*(a.getY() - b.getY()) + normalePlan.getZ()*(a.getZ() - b.getZ())) 
				/ (normalePlan.getX()*(0 - b.getX()) + normalePlan.getY()*(0 - b.getY()) + normalePlan.getZ()*(zValue - b.getZ())); // on considere que le plan passe par le point (0,0,zValue)	
		
		if(tau < 0 || tau > 1) {
			return null;
		}
		
		Vertex res = new Vertex(0,0,0); // z reste a 0 : une seule face sera dessinnee
		res.setX(tau*a.getX()+(1-tau)*b.getX());
		res.setY(tau*a.getY()+(1-tau)*b.getY());
		return res;
	}
	
	
	private List<Polygon> formerPolygones(ArrayList<Vertex[]> segmentsInter) {
		Vertex currentPoint;
		Vertex startingPoint;
		ArrayList<Polygon> polygones = new ArrayList<>();
		ArrayList<Vertex> polygonVertices ;
		
		while(!segmentsInter.isEmpty()) {
			polygonVertices = new ArrayList<>();
			currentPoint = segmentsInter.get(0)[0];
			startingPoint = currentPoint;
			do {
				polygonVertices.add(currentPoint);
				currentPoint = rechercherCommun(segmentsInter, currentPoint, startingPoint);
			} while(!currentPoint.equals(startingPoint));
			polygones.add(new Polygon(Arrays.copyOf(polygonVertices.toArray(), polygonVertices.size(), Vertex[].class)));
			clearSegments(segmentsInter);
		}
		for(Polygon p : polygones) {
			System.out.println(p);
		}
		return polygones;
	}

	
	private Vertex rechercherCommun(ArrayList<Vertex[]> segmentsInter, Vertex currentPoint, Vertex startingPoint) {
		Vertex tmp;
		Vertex[] segment ;
		for(int i = 0 ; i < segmentsInter.size() ; i++) {
			segment = segmentsInter.get(i);
			if(currentPoint.equals(segment[0])) {
				tmp = segment[1];
				segment[0] = null;
				segment[1] = null;
				return tmp;
			}
			else if(currentPoint.equals(segment[1])) {
				tmp = segment[0];
				segment[0] = null;
				segment[1] = null;
				return tmp;
			}
		}
//		for(Vertex [] v : segmentsInter) {
//			if(v[0] == null || v[1] = )
//		}
		return startingPoint;
	}
	
	
	private void clearSegments(ArrayList<Vertex[]> segmentsInter) {
		int maxIdx = segmentsInter.size();
		for(int i = 0 ; i < maxIdx ; i++) {
			if(segmentsInter.get(i)[0] == null) {
				segmentsInter.remove(i);
				maxIdx--;
				i--;
			}
		}
	}


}