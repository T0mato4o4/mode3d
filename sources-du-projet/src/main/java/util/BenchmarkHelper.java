package util;

public class BenchmarkHelper {
    private long time;

    public BenchmarkHelper() {
        time = System.currentTimeMillis();
    }

    public double getSeconds() {
        return ((System.currentTimeMillis() - time) / 1000.0);
    }

    public double getMilliSeconds() {
        return (double) (System.currentTimeMillis() - time);
    }
}
