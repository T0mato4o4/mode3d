package plyParserTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.util.HashMap;

import model.Figure;
import model.Vertex;
import org.junit.jupiter.api.Test;

import model.Polygon;
import plyParser.Parser;
import plyParser.WrongFileFormatException;

public class ParserTest {
    private final File TEST_FILE = new File("ressources/plyReaderTest/validFile/validatorTest1.ply");
    private final File VALID_FILES = new File("ressources/plyReaderTest/validFile");
    private final File INVALID_FILES = new File("ressources/plyReaderTest/invalidFile");

	@Test
	void getFigure() {
        HashMap<String, Double> otherProperties = new HashMap<>();
        otherProperties.put("confidence", 25.0);

        Figure expectedFigure = new Figure();
        expectedFigure.addPolygon(new Polygon(
                new Vertex(-1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(-1.000000,1.000000,1.000000, otherProperties),
                new Vertex(-1.000000,1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(-1.000000,1.000000,1.000000, otherProperties),
                new Vertex(1.000000,1.000000,1.000000, otherProperties),
                new Vertex(1.000000,1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(1.000000,1.000000,1.000000, otherProperties),
                new Vertex(1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(1.000000,-1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(-1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(-1.000000,-1.000000,-1.000000, otherProperties),
                new Vertex(1.000000,-1.000000,1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(-1.000000,-1.000000,-1.000000, otherProperties),
                new Vertex(-1.000000,1.000000,-1.000000, otherProperties),
                new Vertex(1.000000,1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(1.000000,1.000000,1.000000, otherProperties),
                new Vertex(-1.000000,1.000000,1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(-1.000000,-1.000000,-1.000000, otherProperties),
                new Vertex(-1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(-1.000000,1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(-1.000000,1.000000,-1.000000, otherProperties),
                new Vertex(-1.000000,1.000000,1.000000, otherProperties),
                new Vertex(1.000000,1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(1.000000,1.000000,-1.000000, otherProperties),
                new Vertex(1.000000,1.000000,1.000000, otherProperties),
                new Vertex(1.000000,-1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(-1.000000,-1.000000,-1.000000, otherProperties),
                new Vertex(1.000000,-1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(1.000000,-1.000000,-1.000000, otherProperties),
                new Vertex(-1.000000,-1.000000,-1.000000, otherProperties),
                new Vertex(1.000000,1.000000,-1.000000, otherProperties)
        ));
        expectedFigure.addPolygon(new Polygon(
                new Vertex(-1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(1.000000,-1.000000,1.000000, otherProperties),
                new Vertex(-1.000000,1.000000,1.000000, otherProperties)
        ));
		
        Parser parser = new Parser(TEST_FILE);
        

        assertEquals(parser.getFigure(), expectedFigure);
	}

	@Test
    public void validFile() {
        for (File plyFile:
                VALID_FILES.listFiles()) {
            new Parser(plyFile);
        }
    }

    @Test
    public void invalidFile() {
        for (File plyFile:
                INVALID_FILES.listFiles()) {
            assertThrows(WrongFileFormatException.class, ()->{
                new Parser(plyFile);
            });
        }
    }
    
    @Test
    public void parseInformations() {
    	Parser p = new Parser(TEST_FILE);
    	
    	assertEquals(" moi", p.getFigure()
    			.getInformations()
    			.get("auteur"));
    	assertEquals(" 13/12/2001", p.getFigure().getInformations().get("dateCreation"));
    }
}
