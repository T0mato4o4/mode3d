5 règles choisies importantes :
- OnlyOneReturn : permet grandement d'aider à la lisibilité du code, me semble important
- GodClass : avoir une classe trop multi fonction peut rendre le code peu pratique à l'amélioration.
- LooseCoupling : important pour pouvoir changer facilement les structures de données (en cas d'optimisation, par exemple).
- NcssCount : ne pas faire des méthodes / classe trop longues, question de lisibilité et d'amélioration.
- CommentRequired : commenter son code est important puisqu'il permet aux autres (et parfois à soi-même...) de comprendre ce que fait cette  telle ou telle méthode / classe / attribut.

3 règles non choisies :
- LawOfDemeter : avec JavaFX, il me semble compliqué d'appliquer aisément la loi de Demeter
- SystemPrintln : dans notre cas, on a besoin d'informations sur le terminal (un log ne serait pas pratique  du tout)
- CloseResource : dans notre parser on a besoin de garder le fichier ouvert, cela impliquerait de complètement retravailler le parser
